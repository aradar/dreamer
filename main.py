import argparse
import time
from pathlib import Path

import tensorflow as tf
import numpy as np
import PIL.Image

from dream import Dreamer


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    parser.add_argument("--max-dim", type=int, default=500)
    parser.add_argument("--num-steps", type=int, default=10)
    parser.add_argument("--step-size", type=float, default=0.05)
    parser.add_argument("--input-img", type=Path, required=True)
    parser.add_argument("--output-img", type=Path, required=False)

    return parser.parse_args()


def main():
    args = parse_args()

    base_model = tf.keras.applications.InceptionV3(
        include_top=False, weights="imagenet"
    )
    preprocess_func = tf.keras.applications.inception_v3.preprocess_input
    output_layer_names = ["mixed3", "mixed5"]

    dreamer = Dreamer(
        base_model=base_model,
        output_layer_names=output_layer_names,
        preprocess_func=preprocess_func,
    )

    orig_image = PIL.Image.open(args.input_img)
    orig_image.thumbnail((args.max_dim, args.max_dim))
    orig_image = np.array(orig_image)

    start = time.time()
    dream_image = dreamer.dream(
        image=orig_image, steps=args.num_steps, step_size=args.step_size
    )
    end = time.time()

    print(f"generating dream frame took {end - start}s")

    start = time.time()
    dream_image = dreamer.dream(
        orig_image, steps=args.num_steps, step_size=args.step_size
    )
    end = time.time()

    print(f"generating 2. dream frame took {end - start}s")

    if args.output_img:
        with args.output_img.open("wb") as f:
            PIL.Image.fromarray(dream_image).save(f)


if __name__ == "__main__":
    main()
