from typing import List, Callable

import numpy as np
import tensorflow as tf


# algo from https://www.tensorflow.org/tutorials/generative/deepdream


class Dreamer:
    def __init__(
        self,
        base_model: tf.keras.Model,
        output_layer_names: List[str],
        preprocess_func: Callable[[tf.Tensor, str], tf.Tensor],
    ):
        output_layers = [base_model.get_layer(n).output for n in output_layer_names]
        if not output_layers:
            raise ValueError(
                f"Found no layers with the names: {', '.join(output_layer_names)}"
            )

        self._preprocess_func = preprocess_func
        self._model = tf.keras.Model(inputs=base_model.input, outputs=output_layers)

    def dream(
        self, image: np.ndarray, steps: int = 50, step_size: float = 0.01
    ) -> np.ndarray:
        image = tf.keras.applications.inception_v3.preprocess_input(image)
        image = tf.convert_to_tensor(image)
        loss, image = self._dream(image, tf.constant(steps), tf.constant(step_size))
        result = self._deprocess_img(image)
        return result.numpy()

    @tf.function(
        input_signature=(
            tf.TensorSpec(shape=[None, None, 3], dtype=tf.float32),
            tf.TensorSpec(shape=[], dtype=tf.int32),
            tf.TensorSpec(shape=[], dtype=tf.float32),
        )
    )
    def _dream(self, image, steps, step_size):
        loss = tf.constant(0.0)
        for _ in tf.range(steps):
            with tf.GradientTape() as tape:
                tape.watch(image)
                loss = self._calc_loss(image, self._model)

            gradients = tape.gradient(loss, image)
            gradients /= tf.math.reduce_std(gradients) + 1e-8

            image = image + gradients * step_size
            image = tf.clip_by_value(image, -1, 1)

        return loss, image

    @staticmethod
    def _calc_loss(image: np.ndarray, model: tf.keras.Model) -> tf.Tensor:
        image = tf.expand_dims(image, axis=0)
        activations = model(image)
        if len(activations) == 1:
            activations = [activations]

        losses = []
        for act in activations:
            loss = tf.math.reduce_mean(act)
            losses.append(loss)

        return tf.reduce_sum(losses)

    @staticmethod
    def _deprocess_img(image: np.ndarray):
        image = 255 * (image + 1.0) / 2.0
        return tf.cast(image, tf.uint8)
